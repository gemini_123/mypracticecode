package com;

import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;

public class HdfsDirList 
{
	public static void main(String[] args) 
	{
		final String nameNodeUrl= "hdfs://localhost:9000";
		final String dirPath= "/user/sunbeam";
		
		// create configuration object
		Configuration conf =new Configuration();
		conf.set("fs.defaultFS", nameNodeUrl);
		
		// get HDFS access
		try 
		{
			DistributedFileSystem dfs = (DistributedFileSystem)FileSystem.get(conf);
			//list files
			Path path = new Path(dirPath);
			FileStatus[] files = dfs.listStatus(path);
			for(FileStatus file: files)
			{
				System.out.println(file.getPath().getName()+" , "+file.getLen()+" , "+file.getOwner()+" , "+new Date(file.getModificationTime()));
			}
	
		}
		//Close HDFS
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}

}
