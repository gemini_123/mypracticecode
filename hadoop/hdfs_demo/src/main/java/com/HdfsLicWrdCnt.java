package com;


import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;


public class HdfsLicWrdCnt {

	public static void main(String[] args) 
	{
		final String NameNode="hdfs://localhost:9000";
		final String dirpath="/user/sunbeam/LICENSE.txt";
		
		Configuration config=new Configuration();
		config.set("fs.defaultFS", NameNode);
		// get HDFS access
				try 
				{
					DistributedFileSystem dfs = (DistributedFileSystem)FileSystem.get(config);
					Path path = new Path(dirpath);
					
			        FSDataInputStream inputStream = dfs.open(path);
			        Scanner sc=new Scanner(inputStream);
			       
			        Map< String, Integer> hashmap=new HashMap<String, Integer>();
			        String[] words=null;
			        while(sc.hasNext())
			        {
			        	//System.out.println(sc.nextLine());
			        	String contents=sc.nextLine();        
			        	words=contents.split(" ");    
			        	for(String word:words)
				        {
			        		Integer freq=hashmap.get(word);
			        		if(freq==null)
			        		{
			        			hashmap.put(word, 1);
			        		}
			        		else
			        		{
			        			hashmap.put(word, freq+1);
			        		}
				        }    
			        }
			        System.out.println(hashmap);
			        
			        
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				

	}

}
